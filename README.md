# Worldofmusic

### Run the application
To run the application open a console and navigate to the directory of the "worldofmusic.jar" file and enter:

```sh
$ cd path/to/worldofmusic.jar
$ java -jar worldofmusic.jar
```

After the startup is complete the application will be available at your local machine at port 8080.
You can see the app by switching to your browser and enter 

- http://localhost:8080/

If you want to terminate the application close the console or hit ctrl + c.

In order to work as expected the app needs the "worldofmusic.xml" in the same directory as the "wordofmusic.jar" file.
A log file "worldofmusic.log" and the request result "matchingreleases.xml" will be placed in the same folder.

If you want to change this default behaviour you can tell the app at startup which file names/directories it should use instead:

```sh
$ cd path/to/worldofmusic.jar
$ java -jar worldofmusic.jar --app.recordsAsXml="path/to/worldofmusic.xml" --app.matchingReleasesAsXmlTarget="path/to/matchingreleases.xml" --logging.file="path/to/worldofmusic.log"
```

### Two new feature requests

##### The MusicMoz Data is not available anymore - but there is a new fancy online service with nearly the same data but a different XML structure
As there is an interface RecordsDao one could add another class which implements this interface. This new class could act as a web service client instead of reading xml data from the file system. Either we can map the new xml to the old one via XSLT or we import with JAXB new generated class files. With the latter approach we would have to map the old and new generated class files to a common ground which would result in a new layer of the domain. This could be accomplished by the adapter pattern.

##### "WorldOfMusic" wants to have another XML written, which lists all Releases with at least 2 Compact Discs
If this should be displayed in the MatchingReleases one should either add this field in the xsd or if it is not relevant for the previous requirement to get the number of tracks one could split up the xsd. That is to have a base xsd and two others extending it. Besides the data model this change would have an effect on the behaviour of the domain model. Either there could be a new service method like "getAllRecordsWithAtLeastTwoCds" or the existing service function could be expanded by a new parameter.

### TODOS
- Using AOP for cross-cutting concerns like the logging of start, end and duration of a service call
- Split up the application from a monolithic design to several microservices (Spring MVC webapp as frontend, Spring Web Services as backend)

