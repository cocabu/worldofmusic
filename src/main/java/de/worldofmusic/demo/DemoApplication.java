package de.worldofmusic.demo;

import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;

import de.worldofmusic.demo.dao.Marshaller;
import de.worldofmusic.demo.dao.RecordsDao;
import de.worldofmusic.demo.dao.RecordsDaoImpl;
import de.worldofmusic.demo.dao.XmlFileHandler;
import de.worldofmusic.demo.service.RecordsService;
import de.worldofmusic.demo.service.RecordsServiceImpl;
import de.worldofmusic.demo.service.RecordsServiceImplUtil;

@SpringBootApplication
@ComponentScan(basePackages = { "de.worldofmusic.demo" })
@PropertySource("classpath:application.properties")
public class DemoApplication {

	@Autowired
	private Environment env;

	public static void main(String[] args) {

		SpringApplication.run(DemoApplication.class, args);

	}

	@Bean
	public RecordsService recordsService() {
		return new RecordsServiceImpl();
	}

	@Bean
	public RecordsDao recordsDao() {
		return new RecordsDaoImpl();
	}

	@Bean
	public XmlFileHandler xmlFileHandler() {
		String pathToRecords = env.getProperty("app.recordsAsXml");
		String pathToWriteMatchingReleases = env.getProperty("app.matchingReleasesAsXmlTarget");
		return new XmlFileHandler(pathToRecords, pathToWriteMatchingReleases);
	}

	@Bean
	public Marshaller marshaller() {
		return new Marshaller();
	}

	@Bean
	public RecordsServiceImplUtil recordsServiceImplUtil() {
		return new RecordsServiceImplUtil();
	}

	@Bean
	public Jaxb2Marshaller jaxb2Marshaller() {
		Jaxb2Marshaller jaxb2Marshaller = new Jaxb2Marshaller();
		HashMap<String, Object> marshallProperties = new HashMap<>();
		marshallProperties.put("jaxb.formatted.output", true);
		jaxb2Marshaller.setMarshallerProperties(marshallProperties);
		jaxb2Marshaller.setPackagesToScan("de.worldofmusic.demo.model");
		return jaxb2Marshaller;
	}

}
