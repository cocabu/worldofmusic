package de.worldofmusic.demo.exceptions;

public class MarshallerException extends GeneralWorldOfMusicException {
	
	private static final long serialVersionUID = -8698433248278278189L;

	public MarshallerException() {
		super();
	}
	
	public MarshallerException(String msg) {
		super(msg);
	}
	
	public MarshallerException(Throwable t) {
		super(t);
	}
	
	public MarshallerException(String msg, Throwable t) {
		super(msg, t);
	}

}
