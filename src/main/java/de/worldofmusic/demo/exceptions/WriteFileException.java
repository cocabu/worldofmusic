package de.worldofmusic.demo.exceptions;

public class WriteFileException extends GeneralWorldOfMusicException {

	private static final long serialVersionUID = 9114968147461240311L;

	public WriteFileException() {
		super();
	}
	
	public WriteFileException(String msg) {
		super(msg);
	}
	
	public WriteFileException(Throwable t) {
		super(t);
	}
	
	public WriteFileException(String msg, Throwable t) {
		super(msg, t);
	}
	
}
