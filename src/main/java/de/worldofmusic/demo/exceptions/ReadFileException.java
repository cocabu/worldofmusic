package de.worldofmusic.demo.exceptions;

public class ReadFileException extends GeneralWorldOfMusicException {

	private static final long serialVersionUID = 9114968147461240311L;

	public ReadFileException() {
		super();
	}
	
	public ReadFileException(String msg) {
		super(msg);
	}
	
	public ReadFileException(Throwable t) {
		super(t);
	}
	
	public ReadFileException(String msg, Throwable t) {
		super(msg, t);
	}
	
}
