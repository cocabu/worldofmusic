package de.worldofmusic.demo.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * All custom exceptions which could occur in the backend should extend this unchecked exception.
 */
@ResponseStatus(value=HttpStatus.INTERNAL_SERVER_ERROR, reason="Something went wrong.")
public class GeneralWorldOfMusicException extends RuntimeException {

	private static final long serialVersionUID = 9114968147461240311L;

	public GeneralWorldOfMusicException() {
		super();
	}
	
	public GeneralWorldOfMusicException(String msg) {
		super(msg);
	}
	
	public GeneralWorldOfMusicException(Throwable t) {
		super(t);
	}
	
	public GeneralWorldOfMusicException(String msg, Throwable t) {
		super(msg, t);
	}
	
}
