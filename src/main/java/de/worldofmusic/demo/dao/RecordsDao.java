package de.worldofmusic.demo.dao;

import de.worldofmusic.demo.model.MatchingReleases;
import de.worldofmusic.demo.model.Records;

/**
 * This interface should be used to retrieve music record data.
 */
public interface RecordsDao {

	/**
	 * Returns all the available records.
	 */
	Records getRecords();
	
	/**
	 * Saves the matching releases to the data source.
	 */
	void saveMatchingReleases(MatchingReleases matchingReleases);
	
}
