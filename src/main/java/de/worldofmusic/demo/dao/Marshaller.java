package de.worldofmusic.demo.dao;

import javax.xml.bind.JAXBElement;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.xml.transform.StringResult;
import org.springframework.xml.transform.StringSource;

import de.worldofmusic.demo.exceptions.MarshallerException;
import de.worldofmusic.demo.model.MatchingReleases;
import de.worldofmusic.demo.model.Records;

/**
 * This marshaller can be used to marshall {@link MatchingReleases} and to
 * unmarshall {@link Records}
 */
public class Marshaller {

	@Autowired
	private Jaxb2Marshaller jaxb2Marshaller;

	/**
	 * Method to marshall a given {@link MatchingReleases}
	 * 
	 * @param matchingReleases
	 * @return xml string representation of the marshalled object
	 */
	public String marshall(JAXBElement<MatchingReleases> matchingReleases) {

		try {
			StringResult stringResult = new StringResult();
			jaxb2Marshaller.marshal(matchingReleases, stringResult);
			return stringResult.toString();
		} catch (Exception e) {
			throw new MarshallerException("Exception during marshalling MatchingReleases.", e);
		}

	}

	/**
	 * Method to unmarshall a given xml string into a {@link Records} object.
	 * 
	 * @param xml
	 * @return Records
	 */
	public Records unmarshall(String xml) {

		try {
			Records result;
			Object mapped = jaxb2Marshaller.unmarshal(new StringSource(xml));
			result = (Records) mapped;
			return result;
		} catch (Exception e) {
			throw new MarshallerException("Exception during unmarshalling Records.", e);
		}
	}

}
