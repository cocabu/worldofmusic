package de.worldofmusic.demo.dao;

import javax.xml.bind.JAXBElement;
import javax.xml.namespace.QName;

import org.springframework.beans.factory.annotation.Autowired;

import de.worldofmusic.demo.model.MatchingReleases;
import de.worldofmusic.demo.model.Records;

/**
 * This bean reads a xml file and unmarshall its content or marshalls a Java
 * object and saves it.
 */
public class RecordsDaoImpl implements RecordsDao {

	@Autowired
	private XmlFileHandler xmlFileHandler;

	@Autowired
	private Marshaller marshaller;

	/**
	 * Reads the data from the file system. As this is an IO operation it is
	 * synchronized. If the underlying file system allows concurrent readers,
	 * synchronized can deleted.
	 */
	@Override
	public synchronized Records getRecords() {

		Records result;

		String xml = xmlFileHandler.getFileContent();

		result = marshaller.unmarshall(xml);

		return result;
	}

	/**
	 * Writes the data to the file system. As this is an IO wirte operation it
	 * must be synchronized.
	 */
	@Override
	public synchronized void saveMatchingReleases(MatchingReleases matchingReleases) {

		JAXBElement<MatchingReleases> jaxbElement = new JAXBElement<MatchingReleases>(
				new QName("http://worldofmusic/services", "matchingReleases"), MatchingReleases.class,
				matchingReleases);

		String xml = marshaller.marshall(jaxbElement);

		xmlFileHandler.saveToFile(xml);

	}

}
