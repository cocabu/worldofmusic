package de.worldofmusic.demo.dao;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

import de.worldofmusic.demo.exceptions.ReadFileException;
import de.worldofmusic.demo.exceptions.WriteFileException;

/**
 * This class can read or write the textual content of a UTF-8 file from/to the
 * file system. At instantiation, this class needs the paths to the files to be
 * read or written.
 */
public class XmlFileHandler {

	private final String pathToRecords;
	private final String pathToWriteMatchingReleases;

	public XmlFileHandler(String pathToRecords, String pathToWriteMatchingReleases) {
		this.pathToRecords = pathToRecords;
		this.pathToWriteMatchingReleases = pathToWriteMatchingReleases;
	}

	/**
	 * Getting the content of the file from the file system.
	 * 
	 * @return The content of the file as a String.
	 */
	public String getFileContent() {

		String result;

		try {

			byte[] encoded = Files.readAllBytes(Paths.get(pathToRecords));
			result = new String(encoded, StandardCharsets.UTF_8);
			return result;

		} catch (IOException ioe) {
			throw new ReadFileException("Could not read file!", ioe);
		} catch (Exception e) {
			throw new ReadFileException("Could not read file!", e);
		}
	}

	/**
	 * Function to save given xml string to the file system.
	 * 
	 * @param xml
	 *            to save
	 */
	public void saveToFile(String xml) {

		try {
			
			Files.write(Paths.get(pathToWriteMatchingReleases), xml.getBytes());
			
		} catch (IOException ioe) {
			throw new WriteFileException("Could not write file!", ioe);
		} catch (Exception e) {
			throw new WriteFileException("Could not write file!", e);
		}

	}

}
