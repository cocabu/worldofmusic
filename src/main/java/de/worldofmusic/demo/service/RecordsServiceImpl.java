package de.worldofmusic.demo.service;

import java.time.LocalDate;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StopWatch;

import de.worldofmusic.demo.dao.RecordsDao;
import de.worldofmusic.demo.model.MatchingReleases;
import de.worldofmusic.demo.model.Records.Record;

/**
 * This bean implements the {@link RecordsService}.
 */
public class RecordsServiceImpl implements RecordsService {

	private static final Logger LOG = LoggerFactory.getLogger(RecordsServiceImpl.class);

	@Autowired
	private RecordsDao recordsDao;

	@Autowired
	private RecordsServiceImplUtil recordsServiceImplUtil;

	@Override
	public MatchingReleases getReleasesWithTrackMoreThanAndReleaseDateBefore(Integer limit, LocalDate before) {

		try {

			StopWatch stopWatch = new StopWatch(); // TODO outsource the logging
													// and stopwatch to an
													// aspect
			stopWatch.start();

			LOG.info("Caling service: getReleasesWithLimitAndReleaseDateBefore(" + limit + ", " + before + ")");

			MatchingReleases result;
			List<Record> records;
			List<Record> filteredRecords;

			records = recordsDao.getRecords().getRecord();

			filteredRecords = recordsServiceImplUtil.releasesWithTrackLimitAndReleaseDateBefore(limit, before, records);

			result = recordsServiceImplUtil.mapToMatchingReleases(filteredRecords);

			recordsDao.saveMatchingReleases(result);

			stopWatch.stop();
			LOG.info("Service done: getReleasesWithLimitAndReleaseDateBefore(" + limit + ", " + before + ") after "
					+ stopWatch.getTotalTimeMillis() + "ms");

			return result;

		} catch (Exception e) {
			LOG.error("Exception durin service call.", e);
			throw e;
		}
	}

}
