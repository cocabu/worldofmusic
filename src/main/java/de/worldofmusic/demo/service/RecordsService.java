package de.worldofmusic.demo.service;

import java.time.LocalDate;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import de.worldofmusic.demo.model.MatchingReleases;

/**
 * This interface defines all functions a client can use in order to get information of provided music data / records.
 * 
 */
public interface RecordsService {
	
	/**
	 * Gets all releases with two filters: limit and before date<br>
	 * 
	 * If a filter is not set, the corresponding constraint will not be applied.
	 * 
	 * @param limit Integer, if null no constraint set
	 * @param before LocalDate, if null no constraint set
	 * @return MatchingReleaes
	 */
	@Nonnull MatchingReleases getReleasesWithTrackMoreThanAndReleaseDateBefore(@Nullable Integer limit, @Nullable LocalDate before);

}
