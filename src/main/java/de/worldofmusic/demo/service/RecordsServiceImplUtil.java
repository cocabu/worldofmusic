package de.worldofmusic.demo.service;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.Nonnull;

import de.worldofmusic.demo.model.MatchingReleases;
import de.worldofmusic.demo.model.MatchingReleases.Release;
import de.worldofmusic.demo.model.Records.Record;

public class RecordsServiceImplUtil {

	/**
	 * This functions takes a list of records and filters according to the given parameters.
	 * If one filter parameter is not set, the filtering for this parameter is disabled.
	 * If no filter parameters are given, all of the list elements will be returned.
	 * 
	 * @param bound
	 * @param before
	 * @param input
	 * @return new List containing the filtered records
	 */
	List<Record> releasesWithTrackLimitAndReleaseDateBefore(Integer bound, LocalDate before,
			@Nonnull List<Record> input) {

		List<Record> result;

		result = input.stream().filter(
				record -> (bound != null ? record.getTracklisting().getTrack().size() > bound : true) && (before != null
						? (record.getReleasedate() != null ? record.getReleasedate().isBefore(before) : false) : true))
				.collect(Collectors.toList());

		return result;
	}

	/**
	 * The data source and the request result formats are not equal.<br>
	 * Therefore, this function maps the data source to the request result format.
	 * 
	 * @param list of
	 * @return MatchingReleases
	 */
	MatchingReleases mapToMatchingReleases(List<Record> records) {

		MatchingReleases result = new MatchingReleases();

		List<Release> releases = result.getRelease();

		for (Record record : records) {

			Release release = new Release();

			mapRecordToRelease(record, release);

			releases.add(release);

		}

		return result;

	}

	private void mapRecordToRelease(Record record, Release release) {
		release.setName(record.getName());
		release.setTrackCount(record.getTracklisting().getTrack().size());
	}

}
