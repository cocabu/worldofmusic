package de.worldofmusic.demo.mvc;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import de.worldofmusic.demo.service.RecordsService;

@Controller
public class WorldOfMusicController {

	private static final Logger LOG = LoggerFactory.getLogger(WorldOfMusicController.class);

	@Autowired
	private RecordsService recordsService;

	@RequestMapping("/")
	public String handleUserRequest(@ModelAttribute @Valid UserRequest userRequest, BindingResult errors, Model model) {

		LOG.debug("Lower bound is: " + userRequest.getLowerBound());
		LOG.debug("Before date is: " + userRequest.getBefore());

		model.addAttribute("releases", recordsService
				.getReleasesWithTrackMoreThanAndReleaseDateBefore(userRequest.getLowerBound(), userRequest.getBefore())
				.getRelease());

		return "worldofmusic";

	}

}
