package de.worldofmusic.demo.mvc;

import java.time.LocalDate;

import javax.validation.constraints.Min;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.NumberFormat;
import org.springframework.format.annotation.NumberFormat.Style;

/**
 * Model for {@link WorldOfMusicController}.
 */
public class UserRequest {
	
	@NumberFormat(style = Style.NUMBER)
	@Min(value = 0, message="The number of tracks should be a positive number!")
	private Integer lowerBound = 10;
	
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private LocalDate before = LocalDate.of(2001, 1, 1);
	
	public Integer getLowerBound() {
		return lowerBound;
	}

	public void setLowerBound(Integer lowerBound) {
		this.lowerBound = lowerBound;
	}
	
	public LocalDate getBefore() {
		return before;
	}

	public void setBefore(LocalDate before) {
		this.before = before;
	}

}
