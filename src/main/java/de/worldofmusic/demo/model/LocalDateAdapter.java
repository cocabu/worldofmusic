package de.worldofmusic.demo.model;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

/**
 * This adapter can be used to marshall xsd:date type to
 * {@link java.time.LocalDate} and vice versa.
 * So the generated classes by xjc will contain LocalDate class
 * and not the default XmlGregorianCalendar class.
 */
public class LocalDateAdapter {

	private static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy.MM.dd");

	public static LocalDate unmarshal(String v) {

		return LocalDate.parse(v, formatter);

	}

	public static String marshal(LocalDate v) {

		return v.format(formatter);

	}
}
