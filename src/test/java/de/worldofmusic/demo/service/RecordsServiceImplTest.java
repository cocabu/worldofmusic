package de.worldofmusic.demo.service;

import java.time.LocalDate;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import de.worldofmusic.demo.dao.RecordsDao;
import de.worldofmusic.demo.model.Records;
import de.worldofmusic.demo.model.Records.Record;
import mockit.Expectations;
import mockit.Injectable;
import mockit.Tested;
import mockit.integration.junit4.JMockit;

@RunWith(JMockit.class)
public class RecordsServiceImplTest {

	@Tested
	RecordsServiceImpl sut;

	@Injectable
	RecordsDao recordsDao;

	@Injectable
	RecordsServiceImplUtil recordsServiceImplUtil;

	@Test
	public void testGetReleasesWithTrackMoreThanAndReleaseDateBefore() throws Exception {

		// Record
		new Expectations() {
			{
				Records records = recordsDao.getRecords();
				times = 1;
				List<Record> input = records.getRecord();
				List<Record> filtered;
				filtered = recordsServiceImplUtil.releasesWithTrackLimitAndReleaseDateBefore(anyInt, null, input);
				times = 1;
				recordsServiceImplUtil.mapToMatchingReleases(filtered);
				times = 1;
			}
		};

		// Play
		sut.getReleasesWithTrackMoreThanAndReleaseDateBefore(2, LocalDate.now());

	}

	// TODO What if there is an excpetion when loading the records?
	// @Test
	// public void testGetReleasesWithLimitAndReleaseDateBefore_WithException()
	// throws Exception {
	//
	// // Record
	// new Expectations() {{
	// Records records = recordsDao.getRecords(); result = ;
	// }};
	//
	// // Play
	// sut.getReleasesWithLimitAndReleaseDateBefore(new Integer(2),
	// LocalDate.now());
	//
	// }

}
