package de.worldofmusic.demo.service;

import static org.junit.Assert.assertTrue;

import java.time.LocalDate;
import java.util.List;

import org.junit.Test;

import de.worldofmusic.demo.model.Records;
import de.worldofmusic.demo.model.Records.Record;
import uk.co.jemos.podam.api.DataProviderStrategy;
import uk.co.jemos.podam.api.PodamFactory;
import uk.co.jemos.podam.api.PodamFactoryImpl;
import uk.co.jemos.podam.api.RandomDataProviderStrategyImpl;

public class RecordsServiceImplUtilTest {

	RecordsServiceImplUtil sut = new RecordsServiceImplUtil();

	@Test
	public void testReleasesWithTrackMoreThanAndReleaseDateBefore_getAll() throws Exception {

		List<Record> testRecords = generateTestData(11, null);

		Integer limit = null;
		LocalDate before = null;

		List<Record> result = sut.releasesWithTrackLimitAndReleaseDateBefore(limit, before, testRecords);

		assertTrue(result.equals(testRecords));

	}

	@Test
	public void testReleasesWithTrackMoreThanAndReleaseDateBefore_AllHaveMoreThanBound() {

		List<Record> testRecords = generateTestData(11, null);

		Integer bound = 10;
		LocalDate before = null;

		List<Record> result = sut.releasesWithTrackLimitAndReleaseDateBefore(bound, before, testRecords);

		for (Record record : result) {
			assertTrue(record.getTracklisting().getTrack().size() > bound);
		}

	}

	@Test
	public void testReleasesWithTrackMoreThanAndReleaseDateBefore_AllHaveLessThanBound() {

		List<Record> testRecords = generateTestData(10, null);

		Integer bound = 10;
		LocalDate before = null;

		List<Record> result = sut.releasesWithTrackLimitAndReleaseDateBefore(bound, before, testRecords);

		assertTrue(result.isEmpty());

	}

	@Test
	public void testReleasesWithTrackMoreThanAndReleaseDateBefore_AllReleasedBeforeYesterday() {

		List<Record> testRecords = generateTestData(10, LocalDate.now().minusDays(2L));

		Integer bound = null;
		LocalDate before = LocalDate.now().minusDays(1L);

		List<Record> result = sut.releasesWithTrackLimitAndReleaseDateBefore(bound, before, testRecords);

		assertTrue(result.size() == testRecords.size());

	}
	
	@Test
	public void testReleasesWithTrackMoreThanAndReleaseDateBefore_AllReleasedAfterYesterday() {

		List<Record> testRecords = generateTestData(10, LocalDate.now());

		Integer bound = null;
		LocalDate before = LocalDate.now().minusDays(1L);

		List<Record> result = sut.releasesWithTrackLimitAndReleaseDateBefore(bound, before, testRecords);

		assertTrue(result.isEmpty());

	}
	
	@Test
	public void testReleasesWithTrackMoreThanAndReleaseDateBefore_AllReleasedBeforeYesterdayAndAllMoreThan10Tracks() {

		List<Record> testRecords = generateTestData(11, LocalDate.now().minusDays(2L));

		Integer bound = 10;
		LocalDate before = LocalDate.now().minusDays(1L);

		List<Record> result = sut.releasesWithTrackLimitAndReleaseDateBefore(bound, before, testRecords);

		assertTrue(result.size() == testRecords.size());

	}
	
	@Test
	public void testReleasesWithTrackMoreThanAndReleaseDateBefore_AllReleasedAfterYesterdayAndAllLessThan10Tracks() {

		List<Record> testRecords = generateTestData(9, LocalDate.now());

		Integer bound = 10;
		LocalDate before = LocalDate.now().minusDays(1L);

		List<Record> result = sut.releasesWithTrackLimitAndReleaseDateBefore(bound, before, testRecords);

		assertTrue(result.isEmpty());

	}

	private static List<Record> generateTestData(int tracks, LocalDate releaseDate) {

		List<Record> result;

		DataProviderStrategy strategy = new RandomDataProviderStrategyImpl();

		strategy.setDefaultNumberOfCollectionElements(tracks);

		PodamFactory factory = new PodamFactoryImpl(strategy);

		Records testRecord = factory.manufacturePojo(Records.class);

		result = testRecord.getRecord();

		if (releaseDate != null) {
			for (Record record : result) {
				record.setReleasedate(releaseDate);
			}
		}

		return result;
	}

}
