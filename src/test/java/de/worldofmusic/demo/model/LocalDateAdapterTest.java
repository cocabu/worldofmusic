package de.worldofmusic.demo.model;

import static org.junit.Assert.assertTrue;

import java.time.LocalDate;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;

public class LocalDateAdapterTest {
	
	@Test
	public void testUnmarshall() {
		
		String dateString = "1997.08.20";
		
		LocalDate date = LocalDateAdapter.unmarshal(dateString);
		
		assertTrue(date.equals(LocalDate.of(1997, 8, 20)));
		
	}
	
	@Test
	public void testMarshall() {
		
		LocalDate date = LocalDate.of(2017, 8, 30);
		
		String dateAsString = LocalDateAdapter.marshal(date);
		
		assertTrue(StringUtils.equals(dateAsString, "2017.08.30"));
		
	}
	
	
}
