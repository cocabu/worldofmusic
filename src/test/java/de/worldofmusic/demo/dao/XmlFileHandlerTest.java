package de.worldofmusic.demo.dao;

import static org.junit.Assert.assertTrue;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;

import de.worldofmusic.demo.exceptions.ReadFileException;

public class XmlFileHandlerTest {

	final String expected = "TEST";

	@Test
	public void testGetFileContent() {

		final String path = "src/test/resources/test.txt";

		XmlFileHandler sut = new XmlFileHandler(path, null);

		String fileContent = sut.getFileContent();

		assertTrue("String read from file not equal to expected xml!", StringUtils.equals(fileContent, expected));

	}
	
	@Test(expected = ReadFileException.class)
	public void testGetFileContentException() {

		final String path = "src/test/resources/not-existing.txt";

		XmlFileHandler sut = new XmlFileHandler(path, null);

		sut.getFileContent();

	}

}
