package de.worldofmusic.demo.dao;

import org.junit.Test;
import org.junit.runner.RunWith;

import mockit.Expectations;
import mockit.Injectable;
import mockit.Tested;
import mockit.integration.junit4.JMockit;

@RunWith(JMockit.class)
public class RecordsDaoImplTest {
	
	@Tested
	private RecordsDaoImpl sut;
	
	@Injectable
	private XmlFileHandler fileLoader;
	
	@Injectable
	private Marshaller marshaller;
	
	@Test
	public void testGetRecords() throws Exception {
		
		new Expectations() {{
			String xml = fileLoader.getFileContent();
			marshaller.unmarshall(xml);
		}};
		
		sut.getRecords();
		
	}

}
