package de.worldofmusic.demo.dao;

import static org.junit.Assert.assertTrue;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import de.worldofmusic.demo.DemoApplication;
import de.worldofmusic.demo.model.Records;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = DemoApplication.class)
public class MarshallerTest {
	
	@Autowired
	Marshaller sut;
	
	@Test
	public void testUnmarshall() throws Exception {
		
		XmlFileHandler loader = new XmlFileHandler("src/test/resources/worldofmusic-short.xml", null);
		
		String xml = loader.getFileContent();
		
		Records records = sut.unmarshall(xml);
		
		assertTrue(StringUtils.equals(records.getRecord().get(0).getTitle(), "Anthology of American Folk Music, Vol. 1-3 - Disk 1"));
		
	}

}
